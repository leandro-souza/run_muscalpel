# run_muscalpel

## Details to run HostSymbolTable.x

From the repository root you can write in your terminal:

    ./TXL/HostSymbolTable.x Cflow-IDCT_Transplant/Host/cflow-1.4/src/output.c Cflow-IDCT_Transplant/Temp/temp_symbol_table_host.out  > /dev/null 2> errorFile.out

Look errorFile.out in case of the temp_symbol_table_host.out is still not being created. 

## Compiling TXL Programs - the ‘txlc’ Command

txlc -s 4000 <program>.txl


