/* 
 * File:   c.h
 * Author: alex
 *
 * Created on 05 August 2014, 23:19
 */

#ifndef C_H
#define	C_H






extern char *filename;
extern char *canonical_filename;
extern int line_num;

extern int yylex(void);


int get_token();

void
init_lex(int debug_level);

#endif	/* C_H */

