#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <check.h>

#include "InterfaceHeader.h"

/*
 * 
 */


int compare_two_binary_files(FILE *fp1, FILE *fp2) {
    char ch1, ch2;
    int flag = 0;
    while (((ch1 = fgetc(fp1)) != EOF) &&((ch2 = fgetc(fp2)) != EOF)) {
        if (ch1 == ch2) {
            flag = 1;
            continue;
        } else {
            fseek(fp1, -1, SEEK_CUR);
            flag = 0;
            break;
        }
    }
    if (ch1 == EOF) {
        ch2 = fgetc(fp2);
    }

    if ((ch1 != EOF) || (ch2 != EOF)) {
        flag = 0;
    }

    if (flag == 0) {
        return 1;
        //printf("Two files are not equal :  byte poistion at which two files differ is %d\n", ftell(fp1) + 1);

    } else {
        return 0;
        //printf("Two files are Equal\n ", ftell(fp1) + 1);
    }

}




START_TEST(test_1) {
    char * oracle = "/home/alex/Development/GENERATING_RESULTS_FINAL_PROJ/Oracles/OracleIDCTargv.c";
    char * oracle2 = "/home/alex/Development/GENERATING_RESULTS_FINAL_PROJ/Oracles/OracleIDCTargv2.c";
    char * command = "./cflow -o TESTOUTPUT.out -T argcv.c";
    char * resultFile = "TESTOUTPUT.outIDCT";
    //OracleIDCTargv2.c
    
    
    system(command);
    
    FILE * resultsFile = fopen(resultFile, "r");
    FILE * oracleFile = fopen(oracle,"r");
    FILE * oracle2File = fopen(oracle2, "r");
    
    int signal = 0;
    
    
    if(!compare_two_binary_files(resultsFile, oracleFile)){
        signal = 1;
    }
    if(!compare_two_binary_files(resultsFile, oracle2File)){
        signal = 1;
    }
    if(!signal){
        ck_abort();
    }
    fclose(resultsFile);
    fclose(oracleFile);
    fclose(oracle2File);
}
END_TEST


START_TEST(test_2) {
    //char * oracle = "/home/alex/Development/GENERATING_RESULTS_FINAL_PROJ/Oracles/OracleIDCTargv.c";
    char * command = "./cflow -o TESTEMPTY.out -T Empty.c";
    char * resultFile = "TESTEMPTY.outIDCT";
    
    system(command);
    
    FILE * resultsFile = fopen(resultFile, "r");
    char * line = (char *) malloc (1000 * sizeof(char));
    if(fgets(line,1000, resultsFile) != NULL){
        ck_abort();
    }
    
}
END_TEST




START_TEST(test_3) {
    char * oracle = "/home/alex/Development/GENERATING_RESULTS_FINAL_PROJ/Oracles/OracleIDCTOneLine.c";
    char * command = "./cflow -o TESTONELINE.out -T JustOneLine.c";
    char * resultFile = "TESTONELINE.outIDCT";
    
    system(command);
    
    FILE * resultsFile = fopen(resultFile, "r");
    FILE * oracleFile = fopen(oracle,"r");
    
    if(compare_two_binary_files(resultsFile, oracleFile)){
        ck_abort();
    }
    
    fclose(resultsFile);
}
END_TEST






Suite *
money_suite(void) {

    Suite *s = suite_create("GenTransSuite");
    // Core test case 
    TCase *tc_core = tcase_create("Core");
    tcase_set_timeout(tc_core, 3);
    tcase_add_test(tc_core, test_1);
    tcase_add_test(tc_core, test_2);
    tcase_add_test(tc_core, test_3);
    suite_add_tcase(s, tc_core);
    return s;

}

int main(int argc, char** argv) {

    

    int number_failed;
    Suite *s = money_suite();
    SRunner *sr = srunner_create(s);
    
    srunner_set_log(sr, "/home/alex/Development/GENERATING_RESULTS_FINAL_PROJ/TEST_LOG.out");
    
    srunner_run_all(sr, CK_NORMAL);

    number_failed = srunner_ntests_failed(sr);
    srunner_free(sr);
    return (number_failed == 0) ? EXIT_SUCCESS : EXIT_FAILURE;
    //  return (EXIT_SUCCESS);
}



/*
int main() {

    GRAFT_INTERFACE("/home/alex/Development/TestPROJECTS/TESTENCRYPT.c", "/home/alex/Development/TestPROJECTS/PULAPIZDACOAIELE.out", "1234");

}
 */


/*
int main () {
   
    FILE *fin = fopen ("/home/alex/NetBeansProjects/HostExample/input.in", "r");
    FILE *fout = fopen ("output.out", "w");
    
    int *arrayC;
    int length;
    fscanf (fin, "%d", & length);
    arrayC = (int *) malloc ((length + 1) * sizeof (int));
    int i;
    for (i = 0; i < length; i++) {
        fscanf (fin, "%d", & arrayC [i]);
    }
    for (i = 0; i < length; i++) {
        fprintf (fout, "%d ", arrayC [i]);
    }
    
    
    
    //result: 45 39 -104 61 7 -44 42 -20 45 -85
    int length = 10;
    int myArray[10] = { 3, 45, 1, 67, 8, 98, 12, -12, -133, 0 };
    
    
    short *TRANSPLANT_RESULT;
    TRANSPLANT_RESULT = GRAFT_INTERFACE (length, myArray, NULL, NULL);
    return (EXIT_SUCCESS);
}
 */
